import React, {Component} from 'react';
import PropTypes from "prop-types";
import "./Card.scss"

class Card extends Component {

    render() {
        const {name, price, img, color} = this.props.good;
        const {children} = this.props;

        return (
            <div className="card">
                <div className="card__image">
                    <img src={img} alt="goods img"/>
                </div>
                <div className="card__content">
                    <div className="card__content_name">{name}</div>
                    <div>
                        <div>Цвет: {color}</div>
                        <div>Цена: <span className="price">{price}</span>$</div>
                    </div>


                </div>
                {children}
            </div>
        );
    }
}

Card.propTypes = {
    good: PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        color: PropTypes.string,
        img: PropTypes.string,

    })
}

Card.defaultProps = {
    color: "Уточняйте",
    img: "./img/not_found.jpg",
}

export default Card;