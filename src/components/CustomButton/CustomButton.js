import React, {Component} from 'react';
import "./CustomButton.scss"

class CustomButton extends Component {
    render() {
        const {customBtnClass, setFavourite} = this.props;
        return (
            <div className={customBtnClass} onClick={setFavourite}>

            </div>
        );
    }
}

export default CustomButton;