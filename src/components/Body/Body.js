import React, {Component} from 'react';
import CardList from "../CardList/CardList";
import ModalWindow from "../ModalWindow/ModalWindow";
import Button from "../Button/Button";

class Body extends Component {

    state = {
        goods: [],
        goodsInCart: [],
        goodsInFavourite: [],
        showModal: false,
        addedItemId: null
    }

    componentDidMount() {
        this.getGoodsFromJson();
        this.addKeyToLocalStorage("Cart");
        this.addKeyToLocalStorage("Favourite");
        this.getCartFromLocalstorage();
        this.getFavouriteFromLocalstorage();
    }

    componentDidUpdate() {
        const {goodsInCart, goodsInFavourite} = this.state;
        localStorage.setItem("Cart", JSON.stringify(goodsInCart));
        localStorage.setItem("Favourite", JSON.stringify(goodsInFavourite));
    }


    render() {
        const {goods, goodsInCart, goodsInFavourite, showModal, addedItemId} = this.state;

        return (
            <div>
                <CardList goodsData={goods}
                          goodsInCart={goodsInCart}
                          goodsInFavourite={goodsInFavourite}
                          addToCartDialog={this.handleAddClick}
                          setFavourite={this.setFavourite}/>
                {showModal &&
                !goodsInCart.includes(addedItemId) &&
                <ModalWindow closeWindow={this.closeModal}
                             info={"Добавить товар в корзину?"}
                             children={
                                 <>
                                     <Button btnClass="add_btn" btnAction={this.handleOKClick}>OK</Button>
                                     <Button btnClass="add_btn"
                                             btnAction={this.closeModal}>Cancel</Button>
                                 </>}


                />}

                {showModal &&
                goodsInCart.includes(addedItemId) &&
                <ModalWindow closeWindow={this.closeModal}
                             info={"Товар уже в корзине!"}
                             children={
                                 <Button btnClass="add_btn" btnAction={this.closeModal}>Cancel</Button>
                             }
                />}

            </div>
        );
    }

    async getGoodsFromJson() {
        const goodsSource = "./baza.json"
        const receivedGoodsData = await fetch(goodsSource);
        const goodsData = await receivedGoodsData.json();
        this.setState({goods: goodsData})
    }

    addKeyToLocalStorage = (key) => {
        if (!localStorage.getItem(key)) {
            (localStorage.setItem(key, JSON.stringify([])))
        }
    }

    getCartFromLocalstorage = () => {
        const data = JSON.parse(localStorage.getItem("Cart"));
        this.setState({goodsInCart: data})

    }

    getFavouriteFromLocalstorage = () => {
        const data = JSON.parse(localStorage.getItem("Favourite"));
        this.setState({goodsInFavourite: data})
    }

    addToCart = () => {
        const {goodsInCart, addedItemId} = this.state;
        const isIdExist = goodsInCart.includes(addedItemId);

        if (!isIdExist) {
            this.setState({
                goodsInCart: [...goodsInCart, addedItemId],
                addedItemId: null
            })
        }
    }

    handleOKClick = () => {
        this.addToCart();
        this.closeModal();
    }

    showModal = () => {
        this.setState({
            showModal: true,
        })

        document.body.classList.add("modal-open");

    }

    closeModal = () => {
        this.setState({
            showModal: false,
        })

        document.body.classList.remove("modal-open");
    }

    handleAddClick = (id) => {
        this.showModal();
        this.setState({
            addedItemId: id
        })

    }

    setFavourite = (id) => {
        const {goodsInFavourite} = this.state;
        const isIdExist = goodsInFavourite.includes(id);
        if (!isIdExist) {
            this.setState({
                goodsInFavourite: [...goodsInFavourite, id]
            })
        } else {
            const newData = goodsInFavourite.filter(e => e !== id);
            this.setState({
                goodsInFavourite: newData
            })

        }
    }
}


export default Body;