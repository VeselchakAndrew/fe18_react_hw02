import React, {Component} from 'react';
import Card from "../Card/Card";
import Button from "../Button/Button";
import CustomButton from "../CustomButton/CustomButton";
import "./CardList.scss"
import PropTypes from "prop-types";


class CardList extends Component {
    render() {
        const {goodsData, addToCartDialog, goodsInCart, goodsInFavourite, setFavourite} = this.props;

        const cards = goodsData.map(good => (
                <div key={good.vendorCode}>
                    <Card good={good} children={
                        <>
                            <Button btnClass="add_btn" btnAction={() => addToCartDialog(good.vendorCode)}>
                                {goodsInCart.includes(good.vendorCode) ? "Already in cart" : "Add to cart"}
                            </Button>
                            <CustomButton
                                customBtnClass={goodsInFavourite.includes(good.vendorCode)
                                    ? "favourite  favourite_is_checked"
                                    : "favourite"}
                                setFavourite={() => setFavourite(good.vendorCode)}
                            />
                        </>}
                    />
                </div>
            )
        )

        return (
            <div className="page">
                {cards}
            </div>
        );
    }


}

CardList.propTypes = {
    goodsData: PropTypes.array.isRequired,
    goodsInCart: PropTypes.array,
    goodsInFavourite: PropTypes.array,
    addToCartDialog: PropTypes.func.isRequired,
    setFavourite: PropTypes.func.isRequired,
}

CardList.defaultProps = {
    goodsInCart: [],
    goodsInFavourite: [],
}

export default CardList;