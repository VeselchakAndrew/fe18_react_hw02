import React, {PureComponent} from 'react';
import "./Footer.scss";

class Footer extends PureComponent {
    render() {
        return (
            <div className="footer">
                <div className="footer__logo_container">
                    <img src="./logo/adeptus-astartes.svg" alt="Marines Logo"/>
                </div>
                <div className="footer__text">
                    <p> "They shall be my finest warriors, these men who give of themselves to me. Like clay I shall
                        mould
                        them and in the furnace of war I shall forge them. They shall be of iron will and steely sinew.
                        In
                        great armour I shall clad them and with the mightiest weapons shall they be armed. They will be
                        untouched by plague or disease; no sickness shall blight them. They shall have such tactics,
                        strategies and machines that no foe will best them in battle. They are my bulwark against the
                        Terror. They are the Defenders of Humanity. They are my Space Marines... and they shall know no
                        fear."

                    </p>
                    <p className="footer__text_sign">— The Emperor of Mankind</p>
                </div>
            </div>
        );
    }
}

export default Footer;