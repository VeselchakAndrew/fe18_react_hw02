import React, {Component} from 'react';
import PropTypes from "prop-types";
import "./ModalWindow.scss"

class ModalWindow extends Component {
    render() {
        const {closeWindow, info, children} = this.props;

        return (
            <div onClick={closeWindow} className="modalbg">
                <div className="modal" onClick={this.preventCloseModal}>
                    <div className="modal_header">
                        <div className="cross" onClick={closeWindow}></div>
                    </div>
                    <div className="main_content">
                        <div>{info}</div>
                    </div>
                    {children}
                </div>

            </div>
        );
    }

    preventCloseModal = (e) => {
        e.stopPropagation();
    }
}

ModalWindow.propTypes = {
    closeWindow: PropTypes.func.isRequired,
    info: PropTypes.string,
    children: PropTypes.element.isRequired
}

ModalWindow.defaultProps = {
    info: "Modal Window Text"
}



export default ModalWindow;